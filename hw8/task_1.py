# Напишіть декоратор, який вимірює і виводить на екран час виконання функції в секундах.
# Напишіть декоратор, який перетворює всі вхідні параметри функції в строки (str) а також перетворює результат роботи функції на строку

from random import randint
import time


def decorator_duration_time(funtion: object) -> object:
    def wrapper(*args, **kwargs):
        ts = time.time()
        result = funtion(*args, **kwargs)
        te = time.time()
        print(f'Function duration time: {int((te - ts))} second(s)')
        return result

    return wrapper


def to_string(func):
    def wrap(*args, **kwargs):
        new_args = []
        for arg in args:
            new_args.append(str(arg))

        for key, value in kwargs.items():
            kwargs[key] = str(value)

        result = func(*new_args, **kwargs)

        return str(result)

    return wrap


@to_string
def my_function2(a: int, b: int):
    result = a + b
    return result


@decorator_duration_time
def my_function(a: int, b: int):
    time.sleep(randint(1, 11))
    result = a + b
    return result


a = 5
b = 3

res = my_function2(a, b)
print(f"Function my_function2({a}, {b}) return result [ {res} ] as: {type(res)}")
print(time.ctime())
print(my_function(a, b))
print(time.ctime())
print(my_function(b, b))
print(time.ctime())
