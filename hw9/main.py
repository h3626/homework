# Візьміть своє рішення HW8 (гра) і доопрацюйте його таким чином, щоб програма записувала в файл результати гри
# зі збереженням історичних результатів. Файл має містити пронумаровані y у правильному порядку строки з датою та часом,
# фігурами гравця та АІ і вказанням переможця. У файлі має зберігатися вся історія гри за весь час використання програми.
#
#
# Приклад вмісту файлу
#
# 1. 01 Apr 2022 20-31-33 Player - Stone, AI - Lizard, Winner - Player
# 2. 09 May 2022 21-30-57 Player - Lizard, AI - Spock, Winner - Player
# 3. 09 May 2022 21-32-15 Player - Paper, AI - Scissors, Winner - AI
# 4. 10 May 2022 10-18-03 Player - Paper, AI - Scissors, Winner - AI
# 5. 10 May 2022 10-10-00 Player - Paper, AI - Spock, Winner - Player

from datetime import datetime as dt

import lib


def main():
    """
    Returns:
        final_message with result of game
    """

    user_choice = lib.get_user_choice()
    computer_choice = lib.get_computer_choice()
    game_result = lib.get_winner(user_choice, computer_choice)
    final_message = lib.make_message(user_choice, computer_choice, game_result)
    print(final_message)
    return final_message


for i in range(1, 4):
    game_result_str = main()
    now = dt.now()
    log_date = now.strftime("%d %b %Y %H-%M-%S")
    line_number = sum(1 for line in open('log.txt')) + 1
    with open('log.txt', 'a') as file:  # file = open('log.txt')
        file.write(f'{line_number}. {log_date} {game_result_str}\n')
    file.close()
