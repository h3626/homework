# Hапишіть программу "Касир в кінотеатрі", яка буде виконувати наступне:
#
#
#
# Попросіть користувача ввести свсвій вік.
#
# - якщо користувачу менше 7 - вивести "Тобі ж <>! Де твої батьки?"
#
# - якщо користувачу менше 16 - вивести "Тобі лише <>, а це е фільм для дорослих!"
#
# - якщо користувачу більше 65 - вивести "Вам <>? Покажіть пенсійне посвідчення!"
#
# - якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) - вивести "О, вам <>! Який цікавий вік!"
#
# - у будь-якому іншому випадку - вивести "Незважаючи на те, що вам <>, білетів всеодно нема!"
#
#
#
# Замість <> в кожну відповідь підставте значення віку (цифру) та правильну форму слова рік
#
# Для будь-якої відповіді форма слова "рік" має відповідати значенню віку користувача.
#
# Наприклад :
#
# "Тобі ж 5 років! Де твої батьки?"
#
# "Вам 81 рік? Покажіть пенсійне посвідчення!"
#
# "О, вам 33 роки! Який цікавий вік!"
#
#
#
# Зробіть все за допомогою функцій! Для кожної функції пропишіть докстрінг.
#
# Не забувайте що кожна функція має виконувати тільки одну завдання і про правила написання коду.


def check_same_digits(checked_digit: int) -> bool:
    """Check input int value if same digits like 22,33,44 etc.
    Args:
        checked_digit: Integer value that should be > 10
    Returns:
        True if digits are same or do nothing if digit < 10
    """
    if checked_digit > 10:
        str_data = str(checked_digit)
        return str_data.count(str_data[0]) == len(str_data)
    else:
        pass


def check_prefix(age: int) -> str:
    """Check input int value to detect string prefix for out message.
    For example: 22 роки, 1 рік, 10 років
    Args:
        age: Integer value to validate should be > 0
    Returns:
        Str that contain input value and prefix.
    """
    modulo = age % 10
    if modulo == 1 and age < 110:
        return f"{age} рік"
    elif 1 < modulo < 5:
        return f"{age} роки"
    else:
        return f"{age} років"


def check_age(age: int) -> str:
    """Check input int value to return string depending on it.
    For example: "О, вам 33 роки! Який цікавий вік!" або "Тобі ж 5 років! Де твої батьки?"
    Args:
        age: Integer value to validate
    Returns:
        Str that contain out message depended from age.
    """
    prefix = check_prefix(age)
    if age <= 0:
        return "помилка валідації вводу"
    elif check_same_digits(age):
        return f"О, вам {prefix}! Який цікавий вік!"
    elif age < 7:
        return f"Тобі ж {prefix}! Де твої батьки?"
    elif 6 < age < 16:
        return f"Тобі лише {prefix}, а це е фільм для дорослих!"
    elif age > 65:
        return f"Вам {prefix}? Покажіть пенсійне посвідчення!"
    else:
        return f"Незважаючи на те, що вам {prefix}, білетів всеодно нема!"


def start() -> int:
    """Try to return input int value from keyboard or exception if incorrect value while not be a correct.
    Returns:
        Integer value that was input from keyboard.
    """
    while True:
        try:
            return int(input('Введіть Ваш вік ---> '))
        except ValueError:
            print("помилка валідації вводу")
            continue
        break


print(check_age(start()))
