# Напишіть программу "Касир в кінотеватрі", яка буде виконувати наступне:
#
#
#
# Попросіть користувача ввести свсвій вік (можно використати константу або input()).
#
# - якщо користувачу менше 7 - вивести "Де твої батьки?"
#
# - якщо користувачу менше 16 - вивести "Це фільм для дорослих!"
#
# - якщо користувачу більше 65 - вивести "Покажіть пенсійне посвідчення!"
#
# - якщо вік користувача складається з однакових цифр (11, 22, 44 і тд років, всі можливі варіанти!) - вивести "Який цікавий вік!"
#
# - у будь-якому іншому випадку - вивести "А білетів вже немає!"

childrens_out = "Де твої батьки?"
teenagers_out = "Це фільм для дорослих!"
retireds_out = "Покажіть пенсійне посвідчення!"
interesting_age_out = "Який цікавий вік!"
other_out = "А білетів вже немає!"
error_out = "помилка валідації вводу"


def check_same_digits(checked_digit):
    str_data = str(checked_digit)
    return str_data.count(str_data[0]) == len(str_data)


def check_age(age):
    if age <= 0:
        out = error_out
    elif check_same_digits(age):
        out = interesting_age_out
    elif age < 7:
        out = childrens_out
    elif 6 < age < 16:
        out = teenagers_out
    elif age > 65:
        out = retireds_out
    else:
        out = other_out
    return print(f"{out}")


while True:
    try:
        input_age = int(input('Введіть Ваш вік ---> '))
    except ValueError:
        print(f"{error_out}")
        continue
    break

check_age(input_age)
