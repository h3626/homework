# Доопрацюйте класс Point так, щоб в атрибути х та у обʼєктів цього класу можна було записати тільки числа.
# Використовуйте property
# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, що пишуться в точки аналогічно до класу Line. Визначет атрибут, що містить площу
# трикутника


import math


class Point:
    _x = None
    _y = None
    x = property()
    y = property()

    @x.setter
    def x(self, new_val: int):
        """
        Property setter for value 'x' int(value)
        Args:
            new_val: for point 'x'
        """
        if not isinstance(new_val, (int, float)):
            raise TypeError('Only number should be here.')
        self._x = new_val

    @x.getter
    def x(self):
        """
        Property getter for value 'x'
        Returns:
        value 'x'
        """
        return self._x

    @y.setter
    def y(self, new_val: int):
        """
        Property setter for value 'y' int(value)
        Args:
            new_val: for point 'y'
        """
        if not isinstance(new_val, (int, float)):
            raise TypeError('Only number should be here.')
        self._y = new_val

    @y.getter
    def y(self):
        """
        Property getter for value 'y'
        Returns:
        value 'y'
        """
        return self._y

    def __init__(self, x: int, y: int):
        """
        Init method for class Point
        Args:
            x: int(value)
            y: int(value)
        """
        self._x = x
        self._y = y


class Triangle:
    class Line:
        _line = None

        def __init__(self, point_x: Point, point_y: Point):
            self._line = math.sqrt(((point_y.x - point_x.x) ** 2) + ((point_y.y - point_x.y) ** 2))

    _lines = None
    current = 0

    @property
    def square(self):
        """
        Property method that return square of Triangle.
        Returns:
        int(square) as square of Triangle, that calculated by 'Heron’s formula'
        """
        p = (self._lines[0] + self._lines[1] + self._lines[2]) / 2
        side_1 = p - self._lines[0]
        side_2 = p - self._lines[1]
        side_3 = p - self._lines[2]
        return math.sqrt(p * (p - side_1) * (p - side_2) * (p - side_3))

    @property
    def line_a_point_x(self, new_val: Point):
        """
        Property setter for value 'line_a_point_x' Point(value)
        Args:
            new_val: for side 'line_a_point_x'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_a_point_x = self.new_val

    @property
    def line_a_point_y(self, new_val: Point):
        """
        Property setter for value 'line_a_point_y' Point(value)
        Args:
            new_val: for side 'line_a_point_y'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_a_point_y = self.new_val

    @property
    def line_b_point_x(self, new_val: Point):
        """
        Property setter for value 'line_b_point_x' Point(value)
        Args:
            new_val: for side 'line_b_point_x'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_b_point_x = self.new_val

    @property
    def line_b_point_y(self, new_val: Point):
        """
        Property setter for value 'line_b_point_y' Point(value)
        Args:
            new_val: for side 'line_b_point_y'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_b_point_y = self.new_val

    @property
    def line_c_point_x(self, new_val: Point):
        """
        Property setter for value 'line_c_point_x' Point(value)
        Args:
            new_val: for side 'line_c_point_x'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_c_point_x = self.new_val

    @property
    def line_c_point_y(self, new_val: Point):
        """
        Property setter for value 'line_c_point_y' Point(value)
        Args:
            new_val: for side 'line_c_point_y'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self.line_c_point_y = self.new_val

    def __init__(self, line_a_point_x: Point, line_a_point_y: Point, line_b_point_x: Point, line_b_point_y: Point,
                 line_c_point_x: Point, line_c_point_y: Point):
        """
        Init method for class Triangle
        Args:
            line_a_point_x: class Point(value)
            line_a_point_y: class Point(value)
            line_b_point_x: class Point(value)
            line_b_point_y: class Point(value)
            line_c_point_x: class Point(value)
            line_c_point_y: class Point(value)
        """
        self._lines = [self.Line(line_a_point_x, line_a_point_y)._line, self.Line(line_b_point_x, line_b_point_y)._line,
                       self.Line(line_c_point_x, line_c_point_y)._line]

    def __iter__(self):
        self.current = 0
        return self

    def __next__(self):
        if self.current <= len(self._lines):
            self.current += 1
            return self._lines[self.current - 1]
        else:
            raise StopIteration


# start using
point_a = Point(5, 5)
point_b = Point(-3, 2)
point_c = Point(6, 2)
point_d = Point(5, 3)
point_e = Point(2, 7)
point_f = Point(3, 2)

check_triangle = Triangle(point_a, point_b, point_c, point_d, point_e, point_f)

iterator = iter(check_triangle)

print(f'Iterator of Triangle - Current Side = {(next(iterator))}')
print(f'Iterator of Triangle - Current Side = {(next(iterator))}')
print(f'Iterator of Triangle - Current Side = {(next(iterator))}')
print(f'Square of Triangle {check_triangle.square}')
