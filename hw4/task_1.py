# Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

input_str = str(input('Введіть строку зі слів англійською мовою наприклад \'hello my good friend see you soon\' ---> '))
out_list = []
english_vowels = ['A', 'E', 'I', 'O', 'U']
list_of_words = input_str.split(' ')
# print(list_of_words)
for item in list_of_words:
    if item == '' or len(item) == 1:
        continue
    else:
        for letter in english_vowels:
            if (letter + letter).lower() in item.lower():
                print(item)
                out_list.append(item)
print(f"кількість слів, які містять дві голосні літери підряд {len(out_list)}, oсь вони: {out_list}")




