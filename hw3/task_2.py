# Вести з консолі строку зі слів (або скористайтеся константою). Напишіть код, який визначить кількість кількість слів,
# в цих даних.

counter = 0
input_str2 = str(input('Введіть строку зі слів ---> '))

split_str = input_str2.split(' ')
print(split_str)
for item in split_str:
    if item == '':
        continue
    if len(item) == 1:
        if ("-" in item) or ("," in item) or ("." in item) or ("?" in item):  # можно зробити через import re
            continue
    counter += 1
print(f"строка \'{input_str2}\' складаеться з {counter} слів")