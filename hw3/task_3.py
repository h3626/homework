# Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть механізм, який сфлрмує новий list (наприклад lst2), який би містив всі числові змінні, які є в lst1.
# Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 2.5,  9, 0, 'Lorem Ipsum']
lst2 = []
for number in lst1:
    if isinstance(number, int) or isinstance(number, float):
        if isinstance(number, bool):
            continue
        lst2.append(number)
    continue
print(f"list \'{lst1}\' contains list of numbers \'{lst2}\' ")