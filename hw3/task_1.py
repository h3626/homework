# Зформуйте строку, яка містить певну інформацію про символ в відомому слові. Наприклад
# "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

error_out = "Помилка вводу!"
input_str = str(input('Введіть слово ---> '))

while True:
    try:
        input_symbol_number = int(input('Введіть символ ---> '))
    except:
        print(f"{error_out}")
        continue
    if 0 < input_symbol_number <= len(input_str):
        out_str = f"The {input_symbol_number} symbol in \"{input_str}\" is \'{input_str[(input_symbol_number - 1)]}\'"
    else:
        print(f"{error_out} Номер символу повинен будти > 0 та меньше довжини яка = {len(input_str)}")
        continue
    break

print(out_str)