#Задача: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
#Задача: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.
#Задача: Створіть змінну - результат конкатенації (складання) строк str1="Hello " и str2="world". Виведіть на екран.

first = 10
second = 30

addition = first + second
subtraction = second - first
multiplication = second * first
division = second / first
modulo = second % first
floor_division = second // first
exponentiation = second ** first

print(f"first = {first}\nsecond = {second}")
print(f"addition = {addition}")
print(f"subtraction = {subtraction}")
print(f"multiplication = {multiplication}")
print(f"division = {division}")
print(f"modulo = {modulo}")
print(f"floor_division = {floor_division}")
print(f"exponentiation = {exponentiation}")

compare = first > second
print(f"compare (first > second) = {compare}")
compare = first < second
print(f"compare (first < second) = {compare}")
compare = first == second
print(f"compare (first == second) = {compare}")
compare = first != second
print(f"compare (first != second) = {compare}")

str1="Hello "
str2="world"
string_result = str1 + str2
print(f"string_result = {string_result}")