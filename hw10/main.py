# OOP
# Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".
from abc import abstractmethod


def move(direction):
    """
    Make string message about moving
    Args:
        direction:

    Returns:
    (str) message
    """
    return f' I will move {direction}'


class Vehicle:
    color = 'white'
    frame = 'steal'

    def __init__(self, initial_color: object, initial_frame: object) -> object:
        """
        Set default color and frame for subclasses.
        Args:
            initial_color:
            initial_frame:
        """
        self.color = initial_color
        self.frame = initial_frame

    def move(self, direction):
        """
        Make string message about moving
        Args:
            direction:

        Returns:
        (str) message
        """
        return f' I will move {direction}'

class Car(Vehicle):
    vehicle_type = 'Car'
    brand = 'Volvo'

    def descripton(self):
        """
        Make string message about type, color and frame
        Returns:
        (str) message
        """
        return f' This is a {self.vehicle_type}, Brand = {self.brand}, Color = {self.color}, Frame = {self.frame}'


class Airplane(Vehicle):
    vehicle_type = 'Airplane'
    brand = 'Boing'
    model = '747'

    def descripton(self):
        """
           Make string message about type, color and frame
           Returns:
           (str) message
        """
        return f' This is {self.vehicle_type}, Brand = {self.brand}, Model = {self.model}, Color = {self.color}, ' \
               f'Frame = {self.frame}'


class Ship(Vehicle):
    ship_type = 'Cruise'

    def description(self):
        """
           Make string message about type, color and frame
           Returns:
           (str) message
        """
        return f' This is {self.ship_type} liner, Color = {self.color}, Frame = {self.frame}'


car1 = Car(initial_color='Red', initial_frame='Paper')
car2 = Car(initial_color='Black', initial_frame='Aluminum')

print(car1.descripton())
car2.brand = 'Subaru'
print(car2.descripton())
print(move('on the ground'))

airplane1 = Airplane(initial_color='Blue', initial_frame='Wood')
airplane2 = Airplane(initial_color='Green', initial_frame='Steal')

print(airplane1.descripton())
airplane2.vehicle_type = 'Helicopter'
airplane2.brand = 'Bell AH-1'
airplane2.model = 'Cobra'
print(airplane2.descripton())
print(move('in the air'))

ship1 = Ship(initial_color='Grey', initial_frame='Steal')
ship2 = Ship(initial_color='Grey', initial_frame='Wood')

print(move('on the water'))
print(ship1.description())
