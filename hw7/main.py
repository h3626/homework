import lib


def main():
    """
    Returns:
        message with result of game
    """
    for i in range(1, 4):
        user_choice = lib.get_user_choice()
        computer_choice = lib.get_computer_choice()
        game_result = lib.get_winner(user_choice, computer_choice)
        final_message = lib.make_message(user_choice, computer_choice, game_result)
        print(final_message)


main()
