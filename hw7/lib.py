# rock, paper, scissors, lizard, spock
from random import choice

CHOICES = {
    'rock',
    'paper',
    'scissors',
    'lizard',
    'spock',
}

# rules
WIN_RULES = {
    'rock': ['lizard', 'scissors'],
    'paper': ['rock', 'spock'],
    'scissors': ['lizard', 'paper'],
    'lizard': ['spock', 'paper'],
    'spock': ['scissors', 'rock']
}


# get data from user
def get_user_choice():
    """
    Returns:
        String user_choice
    """
    while True:
        user_choice = input(f'Enter your choice {CHOICES}: ')
        if user_choice not in CHOICES:
            print(f'Enter only one of existing choices : {CHOICES}')
            continue
        else:
            return user_choice


# get data from computer
def get_computer_choice():
    """
    Returns:
        String computer_choice
    """
    computer_choice = choice(list(CHOICES))
    return computer_choice


def get_winner(user_choice, computer_choice):
    """
    Returns:
        String game_result
    """
    if user_choice == computer_choice:
        game_result = 'Draw!'
    elif computer_choice in WIN_RULES[user_choice]:
        game_result = 'Winner - Player'
    else:
        game_result = 'Winner - AI'
    return game_result


def make_message(user_choice, computer_choice, game_result):
    """
    Returns:
        String with info about game_result, user_choice, computer_choice
    """
    msg = f'Player - {user_choice}, AI - {computer_choice}, {game_result}'
    return msg
