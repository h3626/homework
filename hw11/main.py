# Доопрацюйте класс Point так, щоб в атрибути х та у обʼєктів цього класу можна було записати тільки числа.
# Використовуйте property
# Створіть класс Triangle (трикутник), який задається трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, що пишуться в точки аналогічно до класу Line. Визначет атрибут, що містить площу
# трикутника


import math


class Point:
    _x = None
    _y = None
    x = property()
    y = property()

    @property
    def length(self: int):
        """
        Property method that return line length.
        Returns:
        int(length) as line length, that sum of points 'x' and 'y'
        """
        return self.x + self.y

    @x.setter
    def x(self, new_val: int):
        """
        Property setter for value 'x' int(value)
        Args:
            new_val: for point 'x'
        """
        if not isinstance(new_val, (int, float)):
            raise TypeError('Only number should be here.')
        self._x = new_val

    @x.getter
    def x(self):
        """
        Property getter for value 'x'
        Returns:
        value 'x'
        """
        return self._x

    @y.setter
    def y(self, new_val: int):
        """
        Property setter for value 'y' int(value)
        Args:
            new_val: for point 'y'
        """
        if not isinstance(new_val, (int, float)):
            raise TypeError('Only number should be here.')
        self._y = new_val

    @y.getter
    def y(self):
        """
        Property getter for value 'y'
        Returns:
        value 'y'
        """
        return self._y

    def __init__(self, x: int, y: int):
        """
        Init method for class Point
        Args:
            x: int(value)
            y: int(value)
        """
        self._x = x
        self._y = y


class Triangle():
    a = property()
    b = property()
    c = property()
    _a = None
    _b = None
    _c = None

    @property
    def square(self):
        """
        Property method that return square of Triangle.
        Returns:
        int(square) as square of Triangle, that calculated by 'Heron’s formula'
        """
        p = (self.a.length + self.b.length + self.c.length) / 2
        return math.sqrt(p * (p - self.a.length) * (p - self.b.length) * (p - self.c.length))

    @a.setter
    def a(self, new_val: Point):
        """
        Property setter for value 'a' Point(value)
        Args:
            new_val: for side 'a'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self._a = new_val

    @a.getter
    def a(self):
        """
        Property getter for value 'a'
        Returns:
        value 'a'
        """
        return self._a

    @b.setter
    def b(self, new_val: Point):
        """
        Property setter for value 'b' Point(value)
        Args:
            new_val: for side 'b'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self._b = new_val

    @b.getter
    def b(self):
        """
        Property getter for value 'b'
        Returns:
        value 'b'
        """
        return self._b

    @c.setter
    def c(self, new_val: Point):
        """
        Property setter for value 'c' Point(value)
        Args:
            new_val: for side 'c'
        """
        if not isinstance(new_val, Point):
            raise TypeError('Only class Point() should be here.')
        self._c = new_val

    @c.getter
    def c(self):
        """
        Property getter for value 'c'
        Returns:
        value 'c'
        """
        return self._c

    def __init__(self, a: Point, b: Point, c: Point):
        """
        Init method for class Triangle
        Args:
            a: class Point(value)
            b: class Point(value)
            c: class Point(value)
        """
        self._a = a
        self._b = b
        self._c = c


# start using
point_a = Point(5, 3)
point_b = Point(4, 7)
point_c = Point(6, 2)

check_triangle = Triangle(point_a, point_b, point_c)

print(f'Side a = {check_triangle.a.length}')
print(f'Side b = {check_triangle.b.length}')
print(f'Side c = {check_triangle.c.length}')
print(f'Square of Triangle {check_triangle.square}')
