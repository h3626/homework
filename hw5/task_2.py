# Напишіть функцію, що приймає один аргумент будь-якого типу та повертає цей аргумент, перетворений на float.
# Якщо перетворити не вдається функція має повернути 0.
import numbers

all_values = [False, 1, 2.2, 'string', {1, 'is', 2}, [1, 2, 3],
              ('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!'),
              {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245,
               "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "sota": 37.720,
               "rozetka": 38.003}]


def convert_to_float(arg):
    if isinstance(arg, numbers.Number):
        return float(arg)
    else:
        return 0


for value in all_values:
    print(f'Variable {value} converted to {convert_to_float(value)}')
