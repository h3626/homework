# Напишіть функцію, що приймає один аргумант. Функція має вивести на ектан тип цього аргумента (використовуйте type)

all_values = [False, 1, 2.2, 'string', {1, 'is', 2}, [1, 2, 3],
              ('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!'),
              {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245,
               "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "sota": 37.720,
               "rozetka": 38.003}]


def return_type_of(arg):
    return type(arg)


for value in all_values:
    print(f'Type of variable \"{value}\" is {return_type_of(value)}')
