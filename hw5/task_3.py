# Напишіть функцію, що приймає два аргументи. Функція повинна
# якщо аргументи відносяться до числових типів - повернути різницю цих аргументів,
# якщо обидва аргументи це строки - обʼєднати в одну строку та повернути
# якщо перший строка, а другий ні - повернути dict де ключ це перший аргумент, а значення - другий
# у будь-якому іншому випадку повернути кортеж з цих аргументів

import numbers

a = 1
b = 2.2
c = 'string'
d = {1, 'is', 2}
e = [1, 2, 3]
f = ('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!')
g = {"cito": 47.999, "BB_studio": 42.999, "momo": 49.999, "main-service": 37.245,
     "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "sota": 37.720,
     "rozetka": 38.003}
h = ' is here'
i = False


def modify_values(arg1, arg2):
    if isinstance(arg1, numbers.Number) and isinstance(arg2, numbers.Number):
        return arg1 - arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return arg1 + arg2
    elif isinstance(arg1, str) and not isinstance(arg2, str):
        return {arg1: arg2}
    else:
        return arg1, arg2


print(f'різниця аргументів {a} та {b} = {modify_values(a, b)}')
print(f'обʼєднані аргументи {c} та {h} = \'{modify_values(c, h)}\'')
print(f'dict з аргументів {c} та {b} = \'{modify_values(c, b)}\'')
print(f'dict з аргументів {c} та {b} = \'{modify_values(c, b)}\'')
print(f'кортеж з цих аргументів {f} та {i} = \'{modify_values(f, i)}\'')

